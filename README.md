Routes exercise - fastest path to nodes (using IPV4 network)

Open source (c) 2018 Presley Lacharmante

Read Licence file

-------------------

Git global setup

git config --global user.name "Sash Informatics (Presley)"

git config --global user.email "sashinformatics@zoho.com"

-------

Create a new repository

git clone git@gitlab.com:sashinformatics/fastestroute.git

cd fastestroute

git push -u origin master

-------

Existing folder

cd existing_folder

git init

git remote add origin git@gitlab.com:sashinformatics/fastestroute.git

git add .

git commit -m "Initial commit"

git push -u origin master

-------

IF MERGE PROBLEM

git pull origin master --allow-unrelated-histories

git merge origin origin/master

... add and commit here...

git push origin master

